<?php
/**
 * Copyright © 2018 RedboxDigital. All rights reserved.
 * 
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'RedboxDigital_Linkedin',
    __DIR__
);
