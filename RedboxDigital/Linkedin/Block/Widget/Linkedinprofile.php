<?php
/**
 * Copyright � 2018 Redbox Digital. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace RedboxDigital\Linkedin\Block\Widget;

use Magento\Customer\Api\CustomerMetadataInterface;

/**
 * Customer Value Added Linkedin URL Widget
 *
 * @SuppressWarnings(PHPMD.DepthOfInheritance)
 */
class Linkedinprofile extends \Magento\Customer\Block\Widget\AbstractWidget
{
    /**
     * Constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Helper\Address $addressHelper
     * @param CustomerMetadataInterface $customerMetadata
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Helper\Address $addressHelper,
        CustomerMetadataInterface $customerMetadata,
        array $data = []
    ) {
        parent::__construct($context, $addressHelper, $customerMetadata, $data);
        $this->_isScopePrivate = true;
    }

    /**
     * Sets the template
     *
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('RedboxDigital_Linkedin::widget/linkedinprofile.phtml');
    }

    /**
     * Get is enabled.
     *
     * @return bool
     */
    public function isEnabled()
    {
    	//return true;
    //	echo "good"; exit;
    	//print_r($this->_getAttribute('linkedin_profile')); exit;
        return $this->_getAttribute('linkedinprofile') ? (bool)$this->_getAttribute('linkedinprofile')->isVisible() : false;
    }

    /**
     * Get is required.
     *
     * @return bool
     */
    public function isRequired()
    {
    	//return true;
        return $this->_getAttribute('linkedinprofile') ? (bool)$this->_getAttribute('linkedinprofile')->isRequired() : false;
    }
}
