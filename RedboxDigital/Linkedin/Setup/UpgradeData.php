<?php
/**
 * Copyright © 2018 RedboxDigital. All rights reserved.
 * 
 */

namespace RedboxDigital\Linkedin\Setup;

use \Magento\Customer\Model\Customer;
use \Magento\Eav\Setup\EavSetup;
use \Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Eav\Model\Entity\Attribute\Set as AttributeSet;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use \Magento\Framework\Setup\UpgradeDataInterface;
use \Magento\Framework\Setup\ModuleContextInterface;
use \Magento\Framework\Setup\ModuleDataSetupInterface;
use \Magento\Framework\Indexer\IndexerRegistry;

class UpgradeData implements UpgradeDataInterface
{
	
	/**
	 * @var AttributeSetFactory
	 */
	private $attributeSetFactory;
	
    /**
     * @param CustomerSetupFactory $customerSetupFactory
     * @param IndexerRegistry $indexerRegistry
     * @param \Magento\Eav\Model\Config $eavConfig
     */
    public function __construct(
        CustomerSetupFactory $customerSetupFactory,
        IndexerRegistry $indexerRegistry,
        \Magento\Eav\Model\Config $eavConfig,
    	AttributeSetFactory $attributeSetFactory
    ) {
        $this->eavSetupFactory = $customerSetupFactory;
        $this->indexerRegistry = $indexerRegistry;
        $this->eavConfig = $eavConfig;
        $this->attributeSetFactory = $attributeSetFactory;
    }

    /**
     * A central function to return the attributes that need to be created
     *
     * 
     **/
    private function getNewAttributes()
    {
        return [
             'linkedinprofile' => [
                'type' => 'static',
                'label' => 'Linkedin Profile URL',
                'input' => 'text',
                'sort_order' => 101,
                'validate_rules' => 'a:1:{s:15:"max_text_length";i:250;}',
              //  'validate_rules' => 'a:2:{s:15:"max_text_length";i:250;s:15:"min_text_length";i:1;}',
                'position' => 101,
                'system' => false,
                'visible' => true,
                'unique' => true,
                'admin_checkout' => 1,
                /* 'adminhtml_only' => 0, --- If the attribute is visible in frontend and backend this line is not required. */
                'is_used_in_grid' => true,
                'is_visible_in_grid' => true,
                'is_filterable_in_grid' => true,
                'is_searchable_in_grid' => false,
            ]
        ];
    }
    /**
     * Installs data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        $attributes = $this->getNewAttributes();

        foreach ($attributes as $code => $options) {
            $eavSetup->addAttribute(
                Customer::ENTITY,
                $code,
                $options
            );
        }

        $this->installCustomerForms($eavSetup);
        
       /* $customerEntity = $eavSetup->getEavConfig()->getEntityType('customer');
        $attributeSetId = $customerEntity->getDefaultAttributeSetId();
        
        /** @var $attributeSet AttributeSet 
        $attributeSet = $this->attributeSetFactory->create();
        $attributeGroupId = $attributeSet->getDefaultGroupId($attributeSetId);
        
        $attribute = $eavSetup->getEavConfig()->getAttribute(Customer::ENTITY, 'linkedinprofile')
        ->addData([
        		'attribute_set_id' => $attributeSetId,
        		'attribute_group_id' => $attributeGroupId,
        		'used_in_forms' => ['adminhtml_customer'],
        		]);
        
        $attribute->save();*/
    }

    /**
     * Add customer attributes to customer forms
     *
     * @param EavSetup $eavSetup
     *
     * @return void
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function installCustomerForms(EavSetup $eavSetup)
    {
        $customer = (int)$eavSetup->getEntityTypeId(\Magento\Customer\Model\Customer::ENTITY);
        /**
         * @var ModuleDataSetupInterface $setup
         */
        $setup = $eavSetup->getSetup();

        $attributeIds = [];
        $select = $setup->getConnection()->select()->from(
            ['ea' => $setup->getTable('eav_attribute')],
            ['entity_type_id', 'attribute_code', 'attribute_id']
        )->where(
            'ea.entity_type_id IN(?)',
            [$customer]
        );
        foreach ($eavSetup->getSetup()->getConnection()->fetchAll($select) as $row) {
            $attributeIds[$row['entity_type_id']][$row['attribute_code']] = $row['attribute_id'];
        }

        $data = [];
        $attributes = $this->getNewAttributes();
        foreach ($attributes as $attributeCode => $attribute) {
            $attributeId = $attributeIds[$customer][$attributeCode];
            $attribute['system'] = isset($attribute['system']) ? $attribute['system'] : true;
            $attribute['visible'] = isset($attribute['visible']) ? $attribute['visible'] : true;
            if ($attribute['system'] != true || $attribute['visible'] != false) {
                $usedInForms = ['customer_account_create', 'customer_account_edit', 'checkout_onepage_register','checkout_onepage_register_guest','checkout_register'];
                if (!empty($attribute['adminhtml_only'])) {
                    $usedInForms = ['adminhtml_customer'];
                } else {
                    $usedInForms[] = 'adminhtml_customer';
                }
                if (!empty($attribute['admin_checkout'])) {
                    $usedInForms[] = 'adminhtml_checkout';
                }
                foreach ($usedInForms as $formCode) {
                    $data[] = ['form_code' => $formCode, 'attribute_id' => $attributeId];
                }
            }
        }
 // more used_in_forms ['adminhtml_checkout','adminhtml_customer','adminhtml_customer_address','customer_account_edit','customer_address_edit','customer_register_address']
        
        if ($data) {
            $setup->getConnection()
                ->insertOnDuplicate($setup->getTable('customer_form_attribute'), $data);
        }

        $indexer = $this->indexerRegistry->get(Customer::CUSTOMER_GRID_INDEXER_ID);
        $indexer->reindexAll();
        $this->eavConfig->clear();
        $setup->endSetup();
    }
}