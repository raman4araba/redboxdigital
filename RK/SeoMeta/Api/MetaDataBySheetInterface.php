<?php 
/**
 * RK_SeoMeta Data extension
 *
 *       @category  Impor
 *       @package   RK_SeoMeta
 *       @author    truelogic.jaipur@gmail.com
 */
namespace RK\SeoMeta\Api;

interface MetaDataBySheetInterface
{
	/**
	 * SEO Meta Data value search by URL key
	 *
	 * @api
	 * @param string $urlKey
	 * @return mixed
	 * @throws \Magento\Framework\Exception\NoSuchEntityException
	 */
	public function getMetaDataByUrl($currentUrl);

}
?>