<?php
namespace RK\SeoMeta\Model;

use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Magento\UrlRewrite\Model\UrlFinderInterface;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use Magento\Cms\Model\PageFactory;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use RK\SeoMeta\Model\CsvImportHandler;
use RK\SeoMeta\Model\SeoMetaData;
/**
 * Class Import
 * @package RK\SeoMeta\Model
 */
class Import
{

    /**
     * @var \RK\SeoMeta\Model\Helper
     */
    private $_sheetHelper;
    
    /**
     * @var \Magento\Framework\View\Page\Config
     */
    protected $_config;
    
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepository;
    
    /**
     * @var \Magento\Catalog\Api\CategoryRepositoryInterface
     */
    protected $categoryRepository;
    
    
    /** @var UrlFinderInterface */
    protected $urlFinder;
    
    public $_storeManager;
    
    protected $pageFactory;
    
    protected $_csvhandler;
    
    protected $_customSeoModel;
    /**
     * 
     */
    public function __construct(
    		\RK\SeoMeta\Helper\Data $sheetHelper,
    		\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
    		\Magento\Framework\View\Page\Config $config,
    		UrlFinderInterface $urlFinder,
    		\Magento\Store\Model\StoreManagerInterface $storeManager,
    		PageFactory $pageFactory,
    		CategoryRepositoryInterface $categoryRepository,
            \RK\SeoMeta\Model\CsvImportHandler $csvhandler,
            SeoMetaData $customModel
    ) {
        $this->_sheetHelper = $sheetHelper;
        $this->_config = $config;
        $this->_productRepository = $productRepository;
        $this->urlFinder = $urlFinder;
        $this->_storeManager=$storeManager;
        $this->pageFactory = $pageFactory;
        $this->categoryRepository = $categoryRepository;
        $this->_csvhandler = $csvhandler;
        $this->_customSeoModel = $customModel;
    }

    
    /**
     * @return bool
     */
    public function execute()
    {
    	//return true;
    	//$spreadsheet_url="https://docs.google.com/spreadsheet/pub?key=1xR-2S73d0V2EwpAxvUv-PWqHRZ9T7QZ1ivOIBuzR998&single=true&gid=0&output=csv";
    	//$spreadsheet_url = $this->_sheetHelper->getGeneralConfig('googlesheet_url');
    	
    	//if(!ini_set('default_socket_timeout', 15))
    		//echo "<!-- unable to change socket timeout -->";
    	$webUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB,true);
    	
    	$collection = $this->_customSeoModel->getCollection();
    	
    	//print_r($collection);
    	
    	foreach ($collection as $item){
    	    if(!empty($item->getUrl())){
    	        //echo $item->getUrl();
    	        $urlKey = str_replace($webUrl,"",$item->getUrl());
    	        if(empty($urlKey))
    	            $urlKey = 'home';
    	        
    	        $rewrite = $this->getRewrite($urlKey,$this->_storeManager->getStore()->getId());
    	        if ($rewrite === null) {
    	            echo "$urlKey".' Not Exist'."\n\r";
    	            continue;
    	        }else{
    	            echo $urlKey."-- Done"."\n\r";
    	        }
    	        $targetPath = $rewrite->getTargetPath();
    	        $entityType = $rewrite->getEntityType();
    	        //return $rewrite->getTargetPath();
    	      //  echo $rewrite->getTargetPath();return true;
    	        switch ($entityType){
    	            
    	            case "cms-page":
    	                $page = $this->pageFactory->create()->load($rewrite->getEntityId());
    	                $page->setMetaTitle($item->getTitle());
    	                $page->setMetaDescription($item->getDescription());
    	                //echo $rewrite->getEntityId();
    	                //return json_encode(array('MetaTitle'=> $page->getMetaTitle(),'MetaDescription' => $page->getMetaDescription(),'Titie' => $page->getTitle(),'canonicalURL' => $this->_googleSeoHelper->getCanonicalUrl($currentUrl)));
    	                break;
    	            case "category":
    	                $category = $this->categoryRepository->get($rewrite->getEntityId(), $this->_storeManager->getStore()->getId());
    	                $category->setMetaTitle($item->getTitle());
    	                $category->setMetaDescription($item->getDescription());
    	                //return json_encode(array('MetaTitle'=> $category->getMetaTitle(),'MetaDescription' => $category->getMetaDescription(),/*'Titie' => $category->getTitle(),'canonicalURL' => $this->_googleSeoHelper->getCanonicalUrl($currentUrl)));
    	                break;
    	            case "product":
    	                $product = $this->_productRepository->getById($rewrite->getEntityId());
    	                $product->setMetaTitle($item->getTitle());
    	                $product->setMetaDescription($item->getDescription());
    	                //return json_encode(array('MetaTitle'=> $product->getMetaTitle(),'MetaDescription' => $product->getMetaDescription(),/*'Titie' => $product->getTitle(),'canonicalURL' => $this->_googleSeoHelper->getCanonicalUrl($currentUrl)));
    	                break;
    	            default:
    	                
    	        }
    	        
    	    }else{
    	        //echo "bad";
    	        echo $item->getUrl()." Not Found \n\r";
    	    }
    	}
    	/*if (($handle = fopen($spreadsheet_url, "r")) !== FALSE) {
    		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
    			
    			$spreadsheet_data[] = $data;
    			if(!empty($data[0])){
	    			$urlKey = str_replace($webUrl,"",$data[0]);
	    			$rewrite = $this->getRewrite($urlKey,$this->_storeManager->getStore()->getId());
	    			if ($rewrite === null) {
	    				return null;
	    			}
	    			$targetPath = $rewrite->getTargetPath();
	    			$entityType = $rewrite->getEntityType();
	    			//return $rewrite->getTargetPath();
	    			switch ($entityType){
	    					
	    				case "cms-page":
	    					$page = $this->pageFactory->create()->load($rewrite->getEntityId());
	    					//echo $rewrite->getEntityId();
	    					//return json_encode(array('MetaTitle'=> $page->getMetaTitle(),'MetaDescription' => $page->getMetaDescription(),'Titie' => $page->getTitle(),'canonicalURL' => $this->_googleSeoHelper->getCanonicalUrl($currentUrl)));
	    					break;
	    				case "category":
	    					$category = $this->categoryRepository->get($rewrite->getEntityId(), $this->_storeManager->getStore()->getId());
	    					//return json_encode(array('MetaTitle'=> $category->getMetaTitle(),'MetaDescription' => $category->getMetaDescription(),/*'Titie' => $category->getTitle(),'canonicalURL' => $this->_googleSeoHelper->getCanonicalUrl($currentUrl)));
	    					break;
	    				case "product":
	    					$product = $this->_productRepository->getById($rewrite->getEntityId());
	    					//return json_encode(array('MetaTitle'=> $product->getMetaTitle(),'MetaDescription' => $product->getMetaDescription(),/*'Titie' => $product->getTitle(),'canonicalURL' => $this->_googleSeoHelper->getCanonicalUrl($currentUrl)));
	    					break;
	    				default:
	    					
	    			}
    			}
    		}
    		fclose($handle);
    		//$ json_encode($spreadsheet_data);
    		return true;
    	}
    	else
    		return false;*/
    	return true;
    }

    
    /**
     * @param string $requestPath
     * @param int $storeId
     * @return UrlRewrite|null
     */
    protected function getRewrite($requestPath, $storeId)
    {
    	return $this->urlFinder->findOneByData([
    			UrlRewrite::REQUEST_PATH => trim($requestPath, '/'),
    			UrlRewrite::STORE_ID => $storeId,
    			]);
    }
    
    
    /**
     * 
     * 
     */
    public function updateDatabase(){
        
    	$spreadsheet_url = $this->_sheetHelper->getGeneralConfig('googlesheet_url');
        return $this->_csvhandler->callImportData($spreadsheet_url);
        
    }
    
}

