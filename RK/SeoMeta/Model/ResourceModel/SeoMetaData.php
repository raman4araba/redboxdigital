<?php
	 
namespace RK\SeoMeta\Model\ResourceModel;
 
 
use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;
	 
class SeoMetaData extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('seo_metadata', 'id');
    }
}