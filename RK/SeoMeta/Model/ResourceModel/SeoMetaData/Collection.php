<?php
 
 
namespace RK\SeoMeta\Model\ResourceModel\SeoMetaData;
 
 
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
 
 
class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
	    'RK\SeoMeta\Model\SeoMetaData',
	    'RK\SeoMeta\Model\ResourceModel\SeoMetaData'
	);
    }
}