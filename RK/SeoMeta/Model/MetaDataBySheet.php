<?php
/**
* RK_SeoMeta extension
*                 
*       @category  Google Sheet Import Meta Data
*       @package   RK_SeoMeta
*       @author    truelogic.jaipur@gmail.com
*/
namespace RK\SeoMeta\Model;

use Magento\UrlRewrite\Model\UrlFinderInterface;
use Magento\UrlRewrite\Service\V1\Data\UrlRewrite;
use Magento\Cms\Model\PageFactory;
use Magento\Catalog\Api\CategoryRepositoryInterface;

class MetaDataBySheet implements \RK\SeoMeta\Api\MetaDataBySheetInterface {
	
	/**
	 * @var \RK\SeoMeta\Helper\Data
	 */
	protected $_googleSeoHelper;
	/**
	 * @var \Magento\Framework\View\Page\Config
	 */
	protected $_config;
	
	/**
	 * @var \Magento\Catalog\Api\ProductRepositoryInterface
	 */
	protected $_productRepository;
	
	/**
	 * @var \Magento\Catalog\Api\CategoryRepositoryInterface
	 */
	protected $categoryRepository;
	
	
	/** @var UrlFinderInterface */
	protected $urlFinder;
	
	public $_storeManager;
	
	protected $pageFactory;
	
	public function __construct(
			\Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
			\RK\SeoMeta\Helper\Data $googleSeoHelper,
			\Magento\Framework\View\Page\Config $config,
			UrlFinderInterface $urlFinder,
			\Magento\Store\Model\StoreManagerInterface $storeManager,
			PageFactory $pageFactory,
			CategoryRepositoryInterface $categoryRepository
	)
	{
		$this->_googleSeoHelper = $googleSeoHelper;
		$this->_config = $config;
		$this->_productRepository = $productRepository;
		$this->urlFinder = $urlFinder;
		$this->_storeManager=$storeManager;
		$this->pageFactory = $pageFactory;
		$this->categoryRepository = $categoryRepository;
	}
	
	/**
	 * {@inheritdoc}
	 */
	public function getMetaDataByUrl($currentUrl)
	{
		
		$webUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB,true);
		$urlKey = str_replace($webUrl,"",$currentUrl); 
		$rewrite = $this->getRewrite($urlKey,$this->_storeManager->getStore()->getId());
		if ($rewrite === null) {
			return null;
		}
		$targetPath = $rewrite->getTargetPath();
		$entityType = $rewrite->getEntityType();
		//return $rewrite->getTargetPath();
		switch ($entityType){
			
			case "cms-page":
				$page = $this->pageFactory->create()->load($rewrite->getEntityId());
				//echo $rewrite->getEntityId();
				return json_encode(array('MetaTitle'=> $page->getMetaTitle(),'MetaDescription' => $page->getMetaDescription(),'Titie' => $page->getTitle(),'canonicalURL' => $this->_googleSeoHelper->getCanonicalUrl($currentUrl)));
				break;
			case "category":
				$category = $this->categoryRepository->get($rewrite->getEntityId(), $this->_storeManager->getStore()->getId());
				return json_encode(array('MetaTitle'=> $category->getMetaTitle(),'MetaDescription' => $category->getMetaDescription(),/*'Titie' => $category->getTitle(),*/'canonicalURL' => $this->_googleSeoHelper->getCanonicalUrl($currentUrl)));
				break;
			case "product":
				$product = $this->_productRepository->getById($rewrite->getEntityId());
				return json_encode(array('MetaTitle'=> $product->getMetaTitle(),'MetaDescription' => $product->getMetaDescription(),/*'Titie' => $product->getTitle(),*/'canonicalURL' => $this->_googleSeoHelper->getCanonicalUrl($currentUrl)));
				break;
			default:
				return "good";
		}
	}
	
	/**
	 * @param string $requestPath
	 * @param int $storeId
	 * @return UrlRewrite|null
	 */
	protected function getRewrite($requestPath, $storeId)
	{
		return $this->urlFinder->findOneByData([
				UrlRewrite::REQUEST_PATH => trim($requestPath, '/'),
				UrlRewrite::STORE_ID => $storeId,
				]);
	}
	
}
