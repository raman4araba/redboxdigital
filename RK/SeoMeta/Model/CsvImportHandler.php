<?php
namespace RK\SeoMeta\Model;
#use Bss\ImportCustomTable\Model\Import\CustomImport\RowValidatorInterface as ValidatorInterface;
use Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;
class CsvImportHandler extends \Magento\ImportExport\Model\Import\Entity\AbstractEntity
{
    const ID = 'id';
    const TITLE = 'title';
    const URL = 'url';
    const DESCRIPTION = 'description';
    const ROBOTS = 'robots';
    const CANONICAL = 'canonical';
    const DATE = 'create_at';
    
    const TABLE_Entity = 'seo_metadata';
    /**
     * Validation failure message template definitions
     *
     * @var array
     */
    
    /*protected $_messageTemplates = [
    ValidatorInterface::ERROR_MESSAGE_IS_EMPTY => 'Message is empty',
    ];*/
     protected $_permanentAttributes = [self::ID];
    /**
     * If we should check column names
     *
     * @var bool
     */
    protected $needColumnCheck = true;
    /**
     * Valid column names
     *
     * @array
     */
    protected $validColumnNames = [
        self::URL,
        self::TITLE,
        self::DESCRIPTION,
        self::ROBOTS,
        self::CANONICAL,
    ];
    /**
     * Need to log in import history
     *
     * @var bool
     */
    protected $logInHistory = true;
    protected $_validators = [];
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_connection;
    protected $_resource;
    
    /**
     * CSV Processor
     *
     * @var \Magento\Framework\File\Csv
     */
    protected $csvProcessor;
    
    private $googleSheetUrl;
    
    /**
     * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
     */
    public function __construct(
    \Magento\Framework\Json\Helper\Data $jsonHelper,
    \Magento\ImportExport\Helper\Data $importExportData,
    \Magento\ImportExport\Model\ResourceModel\Import\Data $importData,
    \Magento\Framework\App\ResourceConnection $resource,
    \Magento\ImportExport\Model\ResourceModel\Helper $resourceHelper,
    \Magento\Framework\Stdlib\StringUtils $string,
    ProcessingErrorAggregatorInterface $errorAggregator,
    \Magento\Framework\File\Csv $csvProcessor
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->_importExportData = $importExportData;
        $this->_resourceHelper = $resourceHelper;
        $this->_dataSourceModel = $importData;
        $this->_resource = $resource;
        $this->_connection = $resource->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        $this->errorAggregator = $errorAggregator;
        $this->csvProcessor = $csvProcessor;
    }
    public function getValidColumnNames()
    {
        return $this->validColumnNames;
    }
    /**
     * Entity type code getter.
     *
     * @return string
     */
    public function getEntityTypeCode()
    {
        return 'seo_metadata';
    }
    /**
     * Row validation.
     *
     * @param array $rowData
     * @param int $rowNum
     * @return bool
     */
    public function validateRow(array $rowData, $rowNum)
    {
        $title = false;
        if (isset($this->_validatedRows[$rowNum])) {
            return !$this->getErrorAggregator()->isRowInvalid($rowNum);
        }
        $this->_validatedRows[$rowNum] = true;
        return !$this->getErrorAggregator()->isRowInvalid($rowNum);
    }
    
    public function callImportData($url){
    	$this->googleSheetUrl = $url;
        return $this->_importData();
    }
    /**
     * Create Advanced message data from raw data.
     *
     * @throws \Exception
     * @return bool Result of operation.
     */
    protected function _importData()
    {
        $this->saveEntity();
        return true;
    }
    
    
    /**
     * Save SEO Meta
     *
     * @return $this
     */
    public function saveEntity()
    {
        $this->saveAndReplaceEntity();
        return $this;
    }
    /**
     * Save and replace data message
     *
     * @return $this
     */
    protected function saveAndReplaceEntity()
    {
        $behavior = 'replace';
        $listTitle = [];
        //$bunch = $data;
        $spreadsheet_url="https://docs.google.com/spreadsheet/pub?key=1xR-2S73d0V2EwpAxvUv-PWqHRZ9T7QZ1ivOIBuzR998&single=true&gid=0&output=csv";
        ///spreadsheets/d/([a-zA-Z0-9-_]+)
        preg_match("/\/spreadsheets\/d\/([a-zA-Z0-9-_]+)/", $this->googleSheetUrl, $output_array);
        $spreadsheet_url = $spreadsheet_url="https://docs.google.com/spreadsheet/pub?key=".$output_array[1]."&single=true&gid=0&output=csv";
        if (($handle = fopen($spreadsheet_url, "r")) !== FALSE) 
        {
            $entityList = [];
            $row = 1;
       // while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            while (($bunch = fgetcsv($handle, 1000, ",")) !== FALSE) {
                
                
                //foreach ($bunch as $rowNum => $rowData) {
                   // print_r($rowData);
                    $rowData = $bunch;
                    if($row == 1){ $row++; continue; }
                    //if (!$this->validateRow($rowData, $rowNum)) {
                        //$this->addRowError(ValidatorInterface::ERROR_TITLE_IS_EMPTY, $rowNum);
                   //     continue;
                   // }
                    /*if ($this->getErrorAggregator()->hasToBeTerminated()) {
                        $this->getErrorAggregator()->addRowToSkip($rowNum);
                        continue;
                    }*/
                    $rowTtile= $rowData[0];
                    $listTitle[] = $rowTtile;
                    $entityList[][] = [
                    //    self::ID => $rowData[self::ID],
                        
                        self::URL => $rowData[0],
                        self::TITLE => $rowData[1],
                        self::DESCRIPTION => $rowData[2],
                        self::ROBOTS => $rowData[3],
                        self::CANONICAL => $rowData[4],
                     //   self::DATE => $rowData[self::DATE],
                    ];
                    //print_r($entityList);
                }
                if (\Magento\ImportExport\Model\Import::BEHAVIOR_REPLACE == $behavior) {
                    if ($listTitle) {
                        if ($this->deleteEntityFinish(array_unique(  $listTitle), self::TABLE_Entity)) {
                            $this->saveEntityFinish($entityList, self::TABLE_Entity);
                        }
                    }
                } elseif (\Magento\ImportExport\Model\Import::BEHAVIOR_APPEND == $behavior) {
                    $this->saveEntityFinish($entityList, self::TABLE_Entity);
                }
           // }
            fclose($handle);
        }
        return $this;
    }
    /**
     * Save message to customtable.
     *
     * @param array $entityData
     * @param string $table
     * @return $this
     */
    protected function saveEntityFinish(array $entityData, $table)
    {
        if ($entityData) {
            $tableName = $this->_connection->getTableName($table);
            $entityIn = [];
            foreach ($entityData as $id => $entityRows) {
                    foreach ($entityRows as $row) {
                        $entityIn[] = $row;
                    }
            }
            if ($entityIn) {
                $this->_connection->insertOnDuplicate($tableName, $entityIn,[
                    self::URL,
                    self::TITLE,
                    self::DESCRIPTION,
                    self::ROBOTS,
                    self::CANONICAL,
           ]);
            }
        }
            return $this;
    }
    
    /**
     * 
     */
    protected function deleteEntityFinish(array $listTitle, $table)
    {
        if ($table && $listTitle) {
            try {
                $this->countItemsDeleted += $this->_connection->delete(
                    $this->_connection->getTableName($table),
                    $this->_connection->quoteInto('url IN (?)', $listTitle)
                    );
                
                return true;
            } catch (\Exception $e) {
                return false;
            }
        } else {
            return false;
        }
    }
    
    /**
     * 
     */
    public function getTableData()
    {
        $myTable = $this->_connection->getTableName(self::TABLE_Entity);
        $sql     = $this->_connection->select()->from(
            ["tn" => $myTable]
            );
        $result  = $this->_connection->fetchAll($sql);
        return $result;
    }
   
}