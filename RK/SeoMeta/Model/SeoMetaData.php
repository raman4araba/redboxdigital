<?php 
namespace RK\SeoMeta\Model;

use Magento\Framework\Model\AbstractModel;

class SeoMetaData extends AbstractModel
{
    protected function _construct()
    {
        $this->_init('RK\SeoMeta\Model\ResourceModel\SeoMetaData');
    }
}