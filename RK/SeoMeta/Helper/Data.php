<?php

namespace RK\SeoMeta\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface ;

class Data extends AbstractHelper
{

	const XML_PATH_GOOGLE_SHEET = 'googlesheet/';

	/**
	 * @var \Magento\Framework\Url
	 */
	protected $_url;
	
	/**
	 * @var \Magento\Framework\App\Config\ScopeConfigInterface
	 */
	protected $_scopeConfig;
	
	public function __construct(
			\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
			\Magento\Framework\Url $url
	){
		$this->_url = $url;
		$this->_scopeConfig = $scopeConfig;
	}
	
	/*
	 * @return bool
	*/
	public function isEnabled($code, $storeId = null)
	{
		return $this->getConfigValue(self::XML_PATH_GOOGLE_SHEET .'general/'. $code, $storeId);
	}
	
	public function getConfigValue($field, $storeId = null)
	{
		return $this->_scopeConfig->getValue(
			$field, ScopeInterface::SCOPE_STORE, $storeId
		);
	}

	
	public function getGeneralConfig($code, $storeId = null)
	{
		return $this->getConfigValue(self::XML_PATH_GOOGLE_SHEET .'general/'. $code, $storeId);
	}
	
	/**
	 * @param $string
	 * @return string
	 */
	public function cleanString($string)
	{
		return strip_tags(addcslashes($string, '"\\/'));
	}
	
	/**
	 * @param $object
	 * @param $type
	 */
	public function checkMetaData($object, $metatitle,$metadesc)
	{
		// Check if object has a no title set, and that default fallbacks are enabled
		if (empty($object->getMetaTitle()))
		{
			$object->setMetaTitle($metatitle);
		}
		// Check if object has a no meta description set, and that default fallbacks are enabled
		if (empty($object->getMetaDescription()) )
		{
			$object->setMetaDescription($metadesc);
		}
	}
	
	/**
	 * @return mixed|string
	 */
	
	public function getCanonicalUrl($currentUrl)
	{
		$url = $this->_url->getRebuiltUrl($currentUrl);
		if ($this->_scopeConfig->getValue('web/seo/use_rewrites',
				\Magento\Store\Model\ScopeInterface::SCOPE_STORE))
		{
			$url = str_replace("/index.php", "", $url);
		}
		if ($url && substr($url, -5) !== '.html' && substr($url, -1) !== '/') {
			$url .= '/';
		}
		return $url;
	}
}