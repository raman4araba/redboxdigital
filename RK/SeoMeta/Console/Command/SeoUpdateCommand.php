<?php
namespace RK\SeoMeta\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

/**
 * Class ImportCommand
 *
 * @package CedricBlondeau\CatalogImportCommand\Console\Command
 */
class SeoUpdateCommand extends Command
{
    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var \Magento\Framework\App\State
     */
    private $state;

    /**
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\App\State $state
    ) {
        $this->objectManager = $objectManager;
        $this->state = $state;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('seo:update')
            ->setDescription('Import or Update Meta Data from Google Sheet')
            ->addArgument('operation', InputArgument::REQUIRED, "Input operation to do: import or update");
          //  ->addOption('images_path', "i", InputOption::VALUE_OPTIONAL, "Images path")
          //  ->addOption('behavior', "b", InputOption::VALUE_OPTIONAL, "Behavior");
        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
    	
    	$import_operation = $input->getArgument('operation');
    	//$output->writeln('<info>The DataBAse was successful.</info>'.$import_operation);
    	
    	$import = $this->getImportModel();
         try {
             
             if(strtolower($import_operation) == 'update'){
                 $result = $import->execute();
                 $output->writeln('<info>SEO Meta Data Updated Successfully.</info>');
             }
             elseif(strtolower($import_operation) == 'import'){
                 $result = $import->updateDatabase();
                 $output->writeln('<info>The Database Connection was successful.</info>');
             }else{
                 $result = false;
             }
	               
	    	

            if ($result) {
                $output->writeln('<info>The import was successful.</info>');
            } else {
                $output->writeln('<error>Import failed.</error>');
               
            }

        }  catch (\InvalidArgumentException $e) {
            $output->writeln('<error>Invalid source.</error>');
        }
    }

    /**
     * @return \RK\SeoMeta\Model\Import
     */
    protected function getImportModel()
    {
        $this->state->setAreaCode('adminhtml');
        return $this->objectManager->create('RK\SeoMeta\Model\Import');
    }
}

