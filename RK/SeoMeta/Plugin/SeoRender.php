<?php
/**
 *
 * @category    RK
 * @package     RK_SeoMeta
 * @license     http://www.ramankatyal.com
 */

namespace RK\SeoMeta\Plugin;

use Magento\Framework\App\Request\Http;
use Magento\Framework\Event\Manager;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Page\Config as PageConfig;
use Magento\Framework\View\Page\Config\Renderer;
use Magento\Search\Helper\Data as SearchHelper;
use Magento\Store\Model\StoreManagerInterface;
use RK\SeoMeta\Helper\Data as HelperData;
use RK\SeoMeta\Model\SeoMetaData;

/**
 * Class SeoBeforeRender
 * @package RK\SeoMeta\Plugin
 */
class SeoRender
{

    /**
     * @var \Magento\Framework\View\Page\Config
     */
    protected $pageConfig;

    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var \RK\SeoMeta\Helper\Data
     */
    protected $helperData;

   
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlBuilder;

   
    /**
     * @var \Magento\Search\Helper\Data
     */
    protected $_searchHelper;

    protected $_customSeoModel;
    
    /**
     * SeoRender constructor.
     * @param \Magento\Framework\View\Page\Config $pageConfig
     * @param \Magento\Framework\App\Request\Http $request
     * @param \RK\SeoMeta\Helper\Data $helpData
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\UrlInterface $urlBuilder
     */
    function __construct(
        PageConfig $pageConfig,
        Http $request,
        HelperData $helpData,
        Registry $registry,
        StoreManagerInterface $storeManager,
        UrlInterface $urlBuilder,
        SeoMetaData $customModel
    )
    {
        $this->pageConfig          = $pageConfig;
        $this->request             = $request;
        $this->helperData          = $helpData;
        $this->_storeManager       = $storeManager;
        $this->_urlBuilder         = $urlBuilder;
        $this->_customSeoModel = $customModel;
    }

    /**
     * @param Renderer $subject
     */
    public function beforeRenderMetadata(Renderer $subject)
    {
        if ($this->helperData->getGeneralConfig('enable')) {
            $pages = [
                'catalogsearch_result_index',
                'cms_noroute_index',
                'catalogsearch_advanced_result'
            ];
            $currentUrl = $this->getCurrentUrl();
            $resultArray = $this->_customSeoModel->getCollection()
                                ->addFieldToFilter('url', array('eq' => $currentUrl))->toArray();
            
                               //print_r($resultArray);
            //if (in_array($this->getFullActionName(), $pages)) {
           if($resultArray[totalRecords]){
               $this->pageConfig->setMetadata('robots', !empty($resultArray['items'][0]['robots'])?$resultArray['items'][0]['robots']:'INDEX,FOLLOW');
               $this->pageConfig->getTitle()->set(!empty($resultArray['items'][0]['title'])?$resultArray['items'][0]['title']:$this->getSeoTitle());
               //$this->pageConfig->getTitle()->set(!empty($resultArray['items'][0]['title'])?$resultArray['items'][0]['title']:$this->getSeoTitle());
               $this->pageConfig->setDescription(!empty($resultArray['items'][0]['description'])?$resultArray['items'][0]['description']:$this->getMetaDescription());
               
               
               //$this->pageConfig->setDescription("Testing");
               $this->pageConfig->addRemotePageAsset(
                   $resultArray['items'][0]['canonical'],
                   'canonical',
                   ['attributes' => ['rel' => 'canonical']]
                   );
            }
        }
    }

  

    /**
     * Get full action name
     * @return string
     */
    public function getFullActionName()
    {
        return $this->request->getFullActionName();
    }

  

    /**
     * Get Url
     * @param string $route
     * @param array $params
     * @return string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->_urlBuilder->getUrl($route, $params);
    }

    /**
     * Get Current URL
     * 
     * @return string
     */
    public function getCurrentUrl(){
    	return $this->_urlBuilder->getCurrentUrl();
    }
}
